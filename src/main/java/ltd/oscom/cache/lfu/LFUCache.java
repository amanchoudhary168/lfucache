package ltd.oscom.cache.lfu;


/**
 * @author aman.choudhary
 *
 * @param <K> Key
 * @param <V> Value
 * 
 * @version 1.0.0
 */

public interface LFUCache<K,V> {
	public void put(K key,V value);
	public V get(K key);
	
}
