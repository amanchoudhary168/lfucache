package ltd.oscom.cache.lfu;

import java.util.HashMap;




/**
 * 
 * This class is an implementation for {@link LFUCache} .
 * 
 * @author aman.choudhary
 *
 * @param <K> Key
 * @param <V> Value
 */
public class LFUCacheImpl<K,V> implements LFUCache<K, V>{

	private static final Integer HEAD = 1;
	private static final Integer TAIL = 2;


	int capacity =0;
	int currentSize=0;
	HashMap<Integer,HashMap<Integer,LinkedListNode<K,V>>> frequencyMap = new HashMap<>();
	HashMap<K,LinkedListNode<K,V>> nodeMap = new HashMap<>();


	int minFrequency = -1;

	public LFUCacheImpl(int size){
		this.capacity = size;
	}

	private static class DataNode<K,V>{
		K key;
		V value;
		int frequency;

		public K getKey() {
			return this.key;
		}
		public void setKey(K key) {
			this.key = key;
		}
		public V getValue() {
			return value;
		}
		public void setValue(V value) {
			this.value = value;
		}
		public int getFrequency() {
			return frequency;
		}
		public void setFrequency(int frequency) {
			this.frequency = frequency;
		}



	}

	private static class LinkedListNode<K,V>{
		DataNode<K,V> data;
		LinkedListNode<K,V> next = null;
		LinkedListNode<K,V> prev = null;

		public DataNode<K,V> getData() {
			return data;
		}
		public void setData(DataNode<K,V> data) {
			this.data = data;
		}
		public LinkedListNode<K,V> getNext() {
			return next;
		}
		public void setNext(LinkedListNode<K,V> next) {
			this.next = next;
		}
		public LinkedListNode<K,V> getPrev() {
			return prev;
		}
		public void setPrev(LinkedListNode<K,V> prev) {
			this.prev = prev;
		}


	}


	@Override
	public void put(K key, V value) {
		if(currentSize>=capacity){
			HashMap<Integer,LinkedListNode<K,V>> map = frequencyMap.get(minFrequency);
			LinkedListNode<K,V> tail = map.get(2);
			nodeMap.remove(tail.getPrev().getData().getKey());
			tail.getPrev().getPrev().setNext(tail);
			tail.setPrev(tail.getPrev().getPrev());
			currentSize--; 
		}

		DataNode<K,V> newNode = new DataNode<>();
		newNode.setValue(value);
		newNode.setKey(key);
		newNode.setFrequency(1);
		addToHead(newNode);
		minFrequency =1;
		currentSize++;
	}




	@Override
	public V get(K key) {

		LinkedListNode<K,V> linkedListNode  = nodeMap.get(key);
		if(linkedListNode == null)
			return null;

		linkedListNode.getNext().setPrev(linkedListNode.getPrev());
		linkedListNode.getPrev().setNext(linkedListNode.getNext());
		nodeMap.remove(key);

		DataNode<K, V> data = linkedListNode.getData();
		minFrequency = Math.max(minFrequency,calculateMinFreq(data.getFrequency()));
		data.setFrequency(data.getFrequency()+1);
		addToHead(data);
		return data.getValue();

	}


	private Integer calculateMinFreq(Integer frequency) {
		
		if(minFrequency == frequency) {
			HashMap<Integer, LinkedListNode<K, V>> headTailMap = frequencyMap.get(frequency);
			if(headTailMap.get(HEAD).next == headTailMap.get(TAIL))
				frequency = frequency+1;
			return frequency;
			
		}
		return -1;
	}

	private void addToHead(DataNode<K,V> newNode) {
		LinkedListNode<K,V> newLinkedNode = new LinkedListNode<>();
		newLinkedNode.setData(newNode);
		nodeMap.put(newNode.getKey(),newLinkedNode);

		HashMap<Integer,LinkedListNode<K,V>> map = frequencyMap.
				getOrDefault(newNode.getFrequency(),new HashMap<Integer,LinkedListNode<K,V>>());

		LinkedListNode<K,V> head = map.get(HEAD);
		LinkedListNode<K,V> tail = map.get(TAIL);


		if(head == null){
			head = new LinkedListNode<K,V>();
			tail = new LinkedListNode<K,V>();

			head.setNext(tail);
			head.setPrev(null);
			tail.setPrev(head);
			tail.setNext(null);

			map.put(HEAD,head);
			map.put(TAIL,tail);
		}

		newLinkedNode.setNext(head.getNext());
		newLinkedNode.setPrev(head);
		head.getNext().setPrev(newLinkedNode);
		head.setNext(newLinkedNode);
		frequencyMap.put(newNode.getFrequency(),map);
	}

}
