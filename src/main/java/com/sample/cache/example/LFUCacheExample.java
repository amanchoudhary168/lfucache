package com.sample.cache.example;

import ltd.oscom.cache.lfu.LFUCache;
import ltd.oscom.cache.lfu.LFUCacheImpl;

public class LFUCacheExample {

	public static void main(String[] args) {
		
		LFUCache<Integer,Integer> lfuCache = new LFUCacheImpl<>(2);
		//Adding elements 2 and 3
		lfuCache.put(2,2);
		lfuCache.put(3,3);
		lfuCache.put(1,1);
		
		System.out.println("get(2) == >"+lfuCache.get(2));
		System.out.println("get(3) == >"+lfuCache.get(3));
		System.out.println("get(1) == >"+lfuCache.get(1));
		System.out.println("get(2) == >"+lfuCache.get(2));
		System.out.println("get(1) == >"+lfuCache.get(1));
		lfuCache.put(4,4);
		System.out.println("get(3) == >"+lfuCache.get(3));
		lfuCache.put(6, 6);
		System.out.println("get(4) == >"+lfuCache.get(4));
		System.out.println("get(6) == >"+lfuCache.get(6));
		System.out.println("get(6) == >"+lfuCache.get(6));
		lfuCache.put(7, 7);
		System.out.println("get(1) == >"+lfuCache.get(1));
		System.out.println("get(6) == >"+lfuCache.get(6));
		System.out.println("get(7) == >"+lfuCache.get(7));
		
		
		

	}

}
