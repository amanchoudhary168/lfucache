import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import ltd.oscom.cache.lfu.LFUCache;
import ltd.oscom.cache.lfu.LFUCacheImpl;

public class LFUCacheTests {
	
	private LFUCache<Integer,Integer> cache;
	
	@Before
	public  void init() {
		cache = new LFUCacheImpl<Integer,Integer>(2);
	}
	
	

	@Test
	public void testInsertionAndFetchingWhenCacheIsEmpty() {
		cache.put(1, 1);
		Integer val = cache.get(1);
		assertEquals(1,val.intValue());
	}
	
	@Test
	public void testInsertionWhenCacheIsNotEmptyAndNotFull() {
		cache.put(1, 1);
		cache.put(2, 2);
		Integer value1 = cache.get(1);
		Integer value2 = cache.get(2);
		assertEquals(1, value1.intValue());
		assertEquals(2, value2.intValue());
		
	}
	
	@Test
	public void testInsertionWhenCacheIsFullAndNoElementVisited() {
		cache.put(1, 1);
		cache.put(2, 2);
		cache.put(3, 3);
		Integer value1 = cache.get(1);
		Integer value2 = cache.get(2);
		Integer value3 = cache.get(3);
		assertNull(value1);
		assertEquals(2, value2.intValue());
		assertEquals(3, value3.intValue());
	}
	
	@Test
	public void testInsertionWhenCacheIsFullAndElementsAreFetchedFromCache() {
		cache.put(1, 1);
		cache.put(2, 2);
		cache.get(1);
		cache.put(3, 3);
		
		
		Integer value1 = cache.get(1);
		Integer value2 = cache.get(2);
		Integer value3 = cache.get(3);
		assertNull(value2);
		assertEquals(1, value1.intValue());
		assertEquals(3, value3.intValue());
		
	}
	
	//@Test
	
	
	@Test
	public void testMultipleInsertionsAndFetchingWithDifferentFrequency() {
		cache.put(1, 1);
		cache.put(2, 2);
		cache.get(1);
		cache.get(1);
		cache.put(3, 3);
		assertNull(cache.get(2));
		assertEquals(1,cache.get(1).intValue());
		assertEquals(3,cache.get(3).intValue());
		cache.put(4, 4);
		assertNull(cache.get(3));
		assertEquals(4,cache.get(4).intValue());
		cache.get(4);
		cache.get(4);
		cache.put(5,5);
		assertNull(cache.get(1));
		assertEquals(5,cache.get(5).intValue());
	}

}
