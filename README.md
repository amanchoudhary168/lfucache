<h3>Description</h3>
This project contains the LFU cache implementation using java.
A person can store and retrieve the data from the cache.If someone tries to store the data in the cache and if the cache is full then the element which is least frequent will be removed.In case if more than one element are least frequent than least recently used data will be removed.
<h3>Technology Used</h3>
    <ul>
        <li>Java 8</li>
        <li>Maven</li>
    </ul>
<h3>Snippet</h3>
    LFUCache<String,Integer> cache = new LFUCacheImpl(2);<br>
    cache.put("A",2);<br>
    System.out.println(cache.get("A"));<br>
<h2>Output</h2>
    2



